# lipton-looper-banners

### Sizmek Upload Instructions
- Compress Files `zip -r 160x600.zip 160x600 -x "*.DS_Store"`
- Sign into [Sizmek MDX](https://platform.mediamind.com/Eyeblaster.ACM.Web/Login/Login.aspx)
- Navigate to "Manage" > "Creative Assets"
- Create a new Folder
- Upload a zip file with your HTML5 assets
- Navigate to "Ads"
- "New" > "Create New Ad"
- Fill in Ad Name and select "HTML5 Polite Banner"
- Find previously created Folder for "Workspace Folder"
- Find "default image" for "Default Image" form field (backup.jpg)
- Enter in Clickthrough URL (ensure it has proper [UTM tagging](https://support.google.com/analytics/answer/1033867?hl=en)) ex: `http://looper.liptontea.com/?utm_source=sizmek&utm_medium=300x250&utm_content=kermit&utm_campaign=liptonlooper`
- Save
- Preview

### Converting GIFs to MP4
+- **Dependencies:** `brew install imagemagick;brew install ffmpeg`
+- `cd dev`
+- place target gif in `/assets/gifs`
+- `sh convert.sh ./assets/gifs`
+- converted MP4 files will be placed in `./converted`
>>>>>>> Stashed changes


[Rich Banner Build Guide](http://demo.sizmek.com/blocks/buildguide/HTML5%20Rich%20Banner%20Build%20Guide.pdf)
[Polite Rich Banner](http://showcase.sizmek.com/formats/demo/html5-rich-banner)
