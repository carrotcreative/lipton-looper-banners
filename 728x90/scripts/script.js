var adDiv;
var videoContainer;
var video;
var sdkVideoPlayer;
var sdkVideoPlayButton;
var isIOS = (/iPhone|iPad|iPod/i).test(navigator.userAgent);

function initEB() {
  if (!EB.isInitialized()) {
    EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
  } else {
    startAd();
  }
}

function startAd() {
  adDiv              = document.getElementById("ad");
  videoContainer     = document.getElementById("video-container");
  video              = document.getElementById("video");
  videoPlay          = document.getElementById("video-play");
  videoPause         = document.getElementById("video-pause");

  addEventListeners();
  initVideo();

  if (isIOS) {
    centerWebkitVideoControls();
  }
}

function addEventListeners() {
  adDiv.addEventListener("click", clickthrough);
  document.getElementById("video-play").addEventListener("click", play);
  document.getElementById("video-pause").addEventListener("click", pause);
}

function initVideo() {
  var sdkData = EB.getSDKData();
  var useSDKVideoPlayer = false;
  var sdkPlayerVideoFormat = "mp4"; // or use "webm" for the webm format

  if (sdkData !== null) {
    if (sdkData.SDKType === "MRAID" && sdkData.version > 1) {
      document.body.classList.add("sdk");

      // set sdk to use custom close button
      EB.setExpandProperties({
        useCustomClose: true
      });

      var sourceTags = video.getElementsByTagName("source");
      var videoSource = "";

      for (var i = 0; i < sourceTags.length; i++) {
        if (sourceTags[i].getAttribute("type")) {
          if (sourceTags[i].getAttribute("type").toLowerCase() === "video/" + sdkPlayerVideoFormat) {
            videoSource = sourceTags[i].getAttribute("src");
          }
        }
      }

      videoContainer.removeChild(video);
      video = null;

      sdkVideoPlayButton.addEventListener("click", function() {
        if (videoSource !== "") {
          EB.playVideoOnNativePlayer(videoSource);
        }
      });

      useSDKVideoPlayer = true;
    }
  }

  videoContainer.style.visibility = "visible";

  if (isIOS) {
    videoPlay.classList.remove("hidden")
  } else {
    video.play();
    videoPause.classList.remove("hidden")
  }

  loops = 1;
  video.onended = function(e) {
    if(loops <= 7) { // 15s / 2s (Video Time) = ~7 (# Allowed Plays)
      video.play();
      loops++;
    } else {
      videoPause.classList.add("hidden")
      videoPlay.classList.remove("hidden")
    }
  };
}

function clickthrough() {
  if (video) {
    video.pause();
  }

  EB.clickthrough();
}

function play(e) {
  e.stopPropagation();

  if(video) {
    videoPlay.classList.add("hidden")
    videoPause.classList.remove("hidden")

    video.loop = true;
    video.play()
  }

  return false;
}

function pause(e) {
  e.stopPropagation();

  if (video) {
    videoPlay.classList.remove("hidden")
    videoPause.classList.add("hidden")

    video.pause();
  }

  return false;
}

function centerWebkitVideoControls() {
  document.body.classList.add("ios-center-video-controls");
}

window.addEventListener("load", initEB);
