var adDiv;
var isIOS = (/iPhone|iPad|iPod/i).test(navigator.userAgent);

function initEB() {
  if (!EB.isInitialized()) {
    EB.addEventListener(EBG.EventName.EB_INITIALIZED, startAd);
  } else {
    startAd();
  }
}

function startAd() {
  adDiv = document.getElementById("ad");
  addEventListeners();
}

function addEventListeners() {
  adDiv.addEventListener("click", clickthrough);
}

function clickthrough() {
  EB.clickthrough();
}

window.addEventListener("load", initEB);
